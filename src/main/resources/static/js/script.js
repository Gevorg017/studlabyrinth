/**
 * @file Этот файл отвечает за отправку пользовательских данных на сервер, их получение и отрисовку маршрутов.
 */

/**
    * Конструктор объекта Point, экземпляры которого содержат информацию о точках.
	*
    * @this {Point}
	* @param {string} _name - название точки
	* @param {number} _x - координата x
	* @param {number} _y - координата y
	* @param {string} _status - статус точки
	*/
function Point(_name, _x, _y, _status)
{
    this.name = _name;
    this.x = _x;
    this.y = _y;
    this.status = _status;
}

let img = new Image();

/**
 * Используются в функции sendPOST.
 * @namespace sendPOSTMembers
 */
sendPOST();

/**
 * Функция, отвечающая за отправку данных из формы на сервер.
 */
function sendPOST()
{
    var tmp = new Array();      // два вспомагательных
    var tmp2 = new Array();     // массива
    var param = new Array();

    /**
     * Получение данных формы из адресной строки
     * @type {string}
     * @memberof sendPOSTMembers
     */
    var get = location.search;  // строка GET запроса, то есть все данные после ?
    if(get != '') {
        tmp = (get.substr(1)).split('&');   // разделяем переменные
        for(var i=0; i < tmp.length; i++) {
            tmp2 = tmp[i].split('=');       // массив param будет содержать
            param[i] = tmp2[1];       // пары ключ(имя переменной)->значение
        }
    }

    param[0] = param[0].replace('+', ' ');
    param[0] = decodeURI(param[0].replace('+', ' '));
    param[1] = decodeURI(param[1].replace('+', ' '));
    param[2] = decodeURI(param[2].replace('+', ' '));

    if (param[1][0] == 'Л' && param[2][0] != 'Л')
    {
    	if (param[2][0] != 'Г' || param[2][0] != 'Б')
    	{
    		let a = param[0][5];
	    	let b = '1';
	    	param[0] = param[0].replace(a, b);
    	}
    	else
    	{
    		let a = param[0][5];
	    	let b = param[2][0];
	    	param[0] = param[0].replace(a, b);
    	}
    }
    else if (param[1][0] != 'Л')
    {
    	if (param[1][0] == 'Г' || param[1][0] == 'Б')
    	{
    		let a = param[0][5];
	        let b = '1';
	        param[0] = param[0].replace(a, b);
    	}
    	else
    	{
    		let a = param[0][5];
	        let b = param[1][0];
	        param[0] = param[0].replace(a, b);
    	}
    }

    if (param[1][0] == param[2][0] || param[1][0] == 'Л' || param[2][0] == 'Л' || (param[1][0] == '1' && param[2][0] == 'Б') || (param[1][0] == '1' && param[2][0] == 'Г')
    	|| (param[2][0] == '1' && param[1][0] == 'Б') || (param[2][0] == '1' && param[1][0] == 'Г') || (param[2][0] == 'Б' && param[1][0] == 'Г') || (param[1][0] == 'Б' && param[2][0] == 'Г'))
    {
        let info = {
            map_name : param[0],
            beginning_point_name : param[1],
            end_point_name : param[2]
        }; 

        let imgSrc = param[0].replace(/\s+/g, '');

        img.src = '/assets/maps/IKIT/' + imgSrc + '.png';

        $('#canvas').css('background', 'url(' + img.src + ')');
        $('#canvas').css('background-size',  '1500px 850px');

        console.log(info);

        /**
         * Тело запроса, отправляемого на сервер.
         * @type {JSON}
         * @memberof sendPOSTMembers
         */
        var body = JSON.stringify(info);

        /**
         * Запрос, отправляемый на сервер.
         * @type {XMLHttpRequest}
         * @memberof sendPOSTMembers
         */
        var xhr = new XMLHttpRequest();

        xhr.open('POST', '/3', true);
        xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        xhr.send(body);

        /**
         * Массив точек, получаемых с сервера.
         * @type {number}
         * @memberof sendPOSTMembers
         */
        var points = {};
        xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                if (xhr.status == 200) {
                    points = JSON.parse(xhr.responseText);
                    

                    for (let i = 0; i < points.length; i++)
                    {
                        points[i].dx = 1500 * (points[i].dx);
                        points[i].dy = 850 * (points[i].dy);
                    }

                    /**
                     * Используются в функции drawWay.
                     * @namespace drawWayMembers
                     */
                    drawWay(points, 'canvas');
                }
            }
        };
        console.log(points[0]);
    }
    else if (param[1][0] != param[2][0] && param[1][0] != 'Л' && param[2][0] != 'Л' && (param[1][0] != '1' && param[2][0] == 'Б') && (param[1][0] != '1' && param[2][0] == 'Г')
    	     && (param[2][0] != '1' && param[1][0] == 'Б') && (param[2][0] != '1' && param[1][0] == 'Г'))
    {
        var info1 = {
            map_name : param[0],
            beginning_point_name : param[1],
            end_point_name : ''
        };

        let imgSrc1 = param[0].replace(/\s+/g, '');
        param[0] = param[0].replace(param[0][5], param[2][0]); 

        var info2 = {
            map_name : param[0],
            beginning_point_name : '',
            end_point_name : param[2]
        };

        let imgSrc2 = param[0].replace(/\s+/g, '');

        img.src = '/assets/maps/IKIT/' + imgSrc1 + '.png';

        $('#canvas').css('background', 'url(' + img.src + ')');
        $('#canvas').css('background-size',  '1500px 850px');

        let newCanvas = document.createElement('canvas');
        newCanvas.id = 'canvas2';
        document.body.appendChild(newCanvas);

        img.src = '/assets/maps/IKIT/' + imgSrc2 + '.png';

        $('#canvas2').attr('width',  '1500px');
        $('#canvas2').attr('height',  '850px');
        $('#canvas2').css('background', 'url(' + img.src + ')');
        $('#canvas2').css('background-size',  '1500px 850px');

        var body = JSON.stringify(info2);

        var xhr1 = new XMLHttpRequest();

        xhr1.open('POST', '/3', true);
        xhr1.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        xhr1.send(body);

        var points1 = {};
        xhr1.onreadystatechange = function () {
            if (xhr1.readyState == XMLHttpRequest.DONE) {
                if (xhr1.status == 200) {
                    points1 = JSON.parse(xhr1.responseText);

                    for (let i = 0; i < points1.length; i++)
                    {
                        points1[i].dx = 1500 * (points1[i].dx );
                        points1[i].dy = 850 * (points1[i].dy );
                    }
                    
                    for (let i = 0; i < points1.length; i++)
                    {
                        if (points1[i].name[0] == 'Л')
                        {
                            info1.end_point_name = points1[i].name;
                        }
                    }


                    body = JSON.stringify(info1);

                    var xhr2 = new XMLHttpRequest();

                    xhr2.open('POST', '/3', true);
                    xhr2.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
                    xhr2.send(body);

                    var points2 = {};
                    xhr2.onreadystatechange = function () {
                        if (xhr2.readyState == XMLHttpRequest.DONE) {
                            if (xhr2.status == 200) {
                                points2 = JSON.parse(xhr2.responseText);

                                for (let i = 0; i < points2.length; i++)
                                {
                                    points2[i].dx = 1500 * (points2[i].dx);
                                    points2[i].dy = 850 * (points2[i].dy);
                                }
                                drawWay(points2, 'canvas');
                                drawWay(points1, 'canvas2');
                            }
                        }
                    };
                }
            }
        };
    }

};

/**
	* Функция, отрисовывающая маршрут на карте.
    *
	* @param {array} points - список точек маршрута
    * @param {string} id - id изображения на котором рисуется маршрут (необходимо в случае отрисовки маршрута между аудиториями на разных этажах)
	*/
function drawWay(points, id)
{
    let canvas = document.getElementById(id);
    /**
     * Контекст canvas, на котором рисуется маршрут.
     * @type {CanvasRenderingContext2D}
     * @memberof drawWayMembers
     */
    let ctx = canvas.getContext('2d');
    ctx.lineWidth = 3;
    ctx.strokeStyle = 'red';

    for (let i = 0; i < points.length; i++)
    {
        if (i == 0)
        {
            ctx.moveTo(points[i].dx, points[i].dy);
        }
        else
        {
            ctx.lineTo(points[i].dx, points[i].dy);
        }
    }
    ctx.stroke();
}

