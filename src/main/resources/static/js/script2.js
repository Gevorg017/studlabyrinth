function Point(_name, _x, _y, _status)
{
    this.name = _name;
    this.x = _x;
    this.y = _y;
    this.status = _status;
}

let img = new Image();

sendPOST();

function sendPOST()
{
    var tmp = new Array();      // два вспомагательных
    var tmp2 = new Array();     // массива
    var param = new Array();

    var get = location.search;  // строка GET запроса, то есть все данные после ?
    if(get != '') {
        tmp = (get.substr(1)).split('&');   // разделяем переменные
        for(var i=0; i < tmp.length; i++) {
            tmp2 = tmp[i].split('=');       // массив param будет содержать
            param[i] = tmp2[1];       // пары ключ(имя переменной)->значение
        }
    }

    param[0] = param[0].replace('+', ' ');
    param[0] = decodeURI(param[0].replace('+', ' '));
    param[1] = decodeURI(param[1].replace('+', ' '));
    param[2] = decodeURI(param[2].replace('+', ' '));

    let info = {
        map_name : param[0],
        beginning_point_name : param[1],
        end_point_name : param[2]
    }; 

    img.src = '/assets/maps/6.jpg';

    $('#canvas').css('background', 'url(' + img.src + ')');
    $('#canvas').css('background-size',  '1500px 850px');

    console.log(info);

    var body = JSON.stringify(info);

    var xhr = new XMLHttpRequest();

    xhr.open('POST', '/3', true);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.send(body);

    var points = {};
    xhr.onreadystatechange = function () {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            if (xhr.status == 200) {
                points = JSON.parse(xhr.responseText);
                alert(xhr.responseText);

                for (let i = 0; i < points.length; i++)
                {
                	points[i].dx = 1500 * points[i].dx;
                	points[i].dy = 850 * points[i].dy;
                }

			    drawWay(points);
            }
        }
    };
};

function drawWay(points)
{
    let canvas2 = document.getElementById('canvas');
    let ctx = canvas2.getContext('2d');
    ctx.lineWidth = 3;
    ctx.strokeStyle = 'red';

    for (let i = 0; i < points.length; i++)
    {
        if (i == 0)
        {
            ctx.moveTo(points[i].dx, points[i].dy);
        }
        else
        {
            ctx.lineTo(points[i].dx, points[i].dy);
        }
    }
    ctx.stroke();
}

