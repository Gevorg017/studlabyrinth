/**
     * При изменении поля с названием корпуса, для каждого элемента поля начальной точки запускается алгоритм, фильтрующий текущий список.
*/
$("#start").children().each(function(index, element) {
    let curL = element.value;
    $("#corp").change(function() {
    $("#start").val(0);
    let curC = document.getElementById('corp').value;
        if (curL[0] != curC[5] && curC != '' && curL != 'Лестница 1' && curL != 'Лестница 2')
        {
            $(element).hide();
            if ((curC[5] == '1' && curL == 'Буфет') || (curC[5] == '1' && curL == 'Главный вход'))
            {
                $(element).show();
            }
        }
        else
        {
            $(element).show();
        }
    });
});

/**
     * При выборе определенной аудитории в точке назначения, она пропадает из списка точек отправления.
*/
$("#start").children().each(function(index, element) {
    let curL = element.value;
    $("#finish").change(function() {
    let curC = document.getElementById('finish').value;
        if (curL == curC && curC != '')
        {
            $(element).hide();
        }
        else if (curL[0] == document.getElementById('corp').value[5])
        {
            $(element).show();
        }
    });
});

/**
     * При выборе определенной аудитории в точке отправления, она пропадает из списка точек назначения.
*/
$("#finish").children().each(function(index, element) {
    let curL = element.value;
    $("#start").change(function() {
    let curC = document.getElementById('start').value;
        if (curL == curC && curC != '')
        {
            $(element).hide();
        }
        else
        {
            $(element).show();
        }
    });
});

$("#corp").change(function() {
    let corp = document.getElementById('corp').options[document.getElementById('corp').selectedIndex].text;
    let start = document.getElementById('start').options[document.getElementById('start').selectedIndex].text;
    let finish = document.getElementById('finish').options[document.getElementById('finish').selectedIndex].text;
    console.log(corp);
    console.log(start);
    console.log(finish);
    if (corp == 'Местонахождение' || start == 'Аудитория/Лестница' || finish == 'Аудитория/Лестница')
    {
        $("#hardButton").attr("disabled", true);
    }
    else
    {
        $("#hardButton").attr("disabled", false );
    }
});

$("#start").change(function() {
    let corp = document.getElementById('corp').options[document.getElementById('corp').selectedIndex].text;
    let start = document.getElementById('start').options[document.getElementById('start').selectedIndex].text;
    let finish = document.getElementById('finish').options[document.getElementById('finish').selectedIndex].text;
    console.log(corp);
    console.log(start);
    console.log(finish);
    if (corp == 'Местонахождение' || start == 'Аудитория/Лестница' || finish == 'Аудитория/Лестница')
    {
        $("#hardButton").attr("disabled", true);
    }
    else
    {
        $("#hardButton").attr("disabled", false );
    }
});

$("#finish").change(function() {
    let corp = document.getElementById('corp').options[document.getElementById('corp').selectedIndex].text;
    let start = document.getElementById('start').options[document.getElementById('start').selectedIndex].text;
    let finish = document.getElementById('finish').options[document.getElementById('finish').selectedIndex].text;
    console.log(corp);
    console.log(start);
    console.log(finish);
    if (corp == 'Местонахождение' || start == 'Аудитория/Лестница' || finish == 'Аудитория/Лестница')
    {
        $("#hardButton").attr("disabled", true);
    }
    else
    {
        $("#hardButton").attr("disabled", false );
    }
});