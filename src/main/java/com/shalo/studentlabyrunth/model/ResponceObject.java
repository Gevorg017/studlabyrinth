package com.shalo.studentlabyrunth.model;

public class ResponceObject {

    private String name;
    private double dx;
    private double dy;
    private String status;

    public ResponceObject() {
    }

    public ResponceObject(String name, double dx, double dy, String status) {
        this.name = name;
        this.dx = dx;
        this.dy = dy;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDx() {
        return dx;
    }

    public void setDx(double dx) {
        this.dx = dx;
    }

    public double getDy() {
        return dy;
    }

    public void setDy(double dy) {
        this.dy = dy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
