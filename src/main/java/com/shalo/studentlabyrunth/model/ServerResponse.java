package com.shalo.studentlabyrunth.model;

import com.shalo.studentlabyrunth.domain.Map;
import com.shalo.studentlabyrunth.domain.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ServerResponse {

    public static List<ResponceObject> answer(List<Point> points, Map map) {
        List<ResponceObject> resp_objects = new ArrayList<>();

        for (Point point : points) {
            ResponceObject obj = new ResponceObject();
            obj.setName(point.getName());
            obj.setDx((double)point.getX() / (double)map.getLength() );
            obj.setDy((double)point.getY() / (double)map.getWidth());
            obj.setStatus(point.getStatus());
            resp_objects.add(obj);
        }

        return resp_objects;

    }
}
