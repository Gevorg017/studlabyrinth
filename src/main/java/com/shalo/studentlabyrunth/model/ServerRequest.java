package com.shalo.studentlabyrunth.model;

public class ServerRequest {
    private String map_name;
    private String beginning_point_name;
    private String end_point_name;

    public ServerRequest() {
    }

    public ServerRequest(String map_name, String beginning_point_name, String end_point_name) {
        this.map_name = map_name;
        this.beginning_point_name = beginning_point_name;
        this.end_point_name = end_point_name;
    }

    public String getMap_name() {
        return map_name;
    }

    public void setMap_name(String map_name) {
        this.map_name = map_name;
    }

    public String getBeginning_point_name() {
        return beginning_point_name;
    }

    public void setBeginning_point_name(String beginning_point_name) {
        this.beginning_point_name = beginning_point_name;
    }

    public String getEnd_point_name() {
        return end_point_name;
    }

    public void setEnd_point_name(String end_point_name) {
        this.end_point_name = end_point_name;
    }
}
