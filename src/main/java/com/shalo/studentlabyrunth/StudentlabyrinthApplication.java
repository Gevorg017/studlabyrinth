package com.shalo.studentlabyrunth;

import com.shalo.studentlabyrunth.domain.Map;
import com.shalo.studentlabyrunth.domain.Way;
import com.shalo.studentlabyrunth.model.ServerResponse;
import com.shalo.studentlabyrunth.repos.MapRepo;
import com.shalo.studentlabyrunth.repos.PointRepo;
import com.shalo.studentlabyrunth.repos.WayRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentlabyrinthApplication {


	public static void main(String[] args) {
		SpringApplication.run(StudentlabyrinthApplication.class, args);
	}

}
