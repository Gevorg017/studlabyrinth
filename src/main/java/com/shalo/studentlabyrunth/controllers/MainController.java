package com.shalo.studentlabyrunth.controllers;

import com.shalo.studentlabyrunth.domain.Map;
import com.shalo.studentlabyrunth.domain.Point;
import com.shalo.studentlabyrunth.domain.Way;
import com.shalo.studentlabyrunth.model.ResponceObject;
import com.shalo.studentlabyrunth.model.ServerRequest;
import com.shalo.studentlabyrunth.model.ServerResponse;
import com.shalo.studentlabyrunth.repos.MapRepo;
import com.shalo.studentlabyrunth.repos.PointRepo;
import com.shalo.studentlabyrunth.repos.WayRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Controller
public class MainController {

    @Autowired
    private MapRepo mapRepo;

    @Autowired
    private WayRepo wayRepo;

    @Autowired
    private PointRepo pointRepo;

    @GetMapping("/1")
    public String first() {
        return  "1";
    }


    @GetMapping("/2")
    public String second() {
        return  "2";
    }


    @GetMapping("/2.1")
    public String second_dot_one() {
        return  "2.1";
    }



    @GetMapping("/3")
    public String third() {
        return  "3";
    }

    @GetMapping("/3.1")
    public String third_dot_one() {
        return  "3.1";
    }

    @GetMapping("/info")
    public String _info() {
        return  "info";
    }

    @PostMapping(value = "/3")
    @ResponseBody
    public List<ResponceObject> getPoints(@RequestBody ServerRequest request) {

        Map current_map = mapRepo.findByName(request.getMap_name());
        Iterable<Map> m = mapRepo.findAll();
        Way current_way = current_map.findWayByTwoPointNames(request.getBeginning_point_name(), request.getEnd_point_name());

        List<Point> points = pointRepo.findByMapID(current_way.getId());

        return ServerResponse.answer(points,current_map);
    }

    @PostMapping(value = "/3.1")
    @ResponseBody
    public List<ResponceObject> getPoint(@RequestBody ServerRequest request) {

        Map current_map = mapRepo.findByName(request.getMap_name());

        Way current_way = current_map.findWayByTwoPointNames(request.getBeginning_point_name(), request.getEnd_point_name());

        List<Point> points = pointRepo.findByMapID(current_way.getId());

        return ServerResponse.answer(points,current_map);
    }
}
