package com.shalo.studentlabyrunth.domain;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class WayTest {

    @Test
    void calcLength() {
        Way way = new Way();
        List<Point> points = new ArrayList<>();
        points.add(new Point(4,2));
        points.add(new Point(1,3));
        points.add(new Point(10,5));
        points.add(new Point(24,11));

        way.setPoints(points);

        assertEquals(27,way.calcLength());

    }
}