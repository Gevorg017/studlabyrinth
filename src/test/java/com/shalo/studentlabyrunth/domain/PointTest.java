package com.shalo.studentlabyrunth.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {

    @Test
    void calcDistance() {
        Point point1 = new Point(10,5);
        Point point2 = new Point(24,11);

        assertEquals(15,point1.calcDistance(point2));
    }
}